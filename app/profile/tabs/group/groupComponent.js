
//Group controller
app.controller('GroupCrtl', ["zmProfileGroupsService", "$scope", "$rootScope", "$filter", function CountCtrl(zmProfileGroupsService, $scope, $rootScope, $filter) {
       zmProfileGroupsService.readListOfGroups().then(function (response) {
           $scope.groups = response.data;
           //function for drop the user in group
            $rootScope.dropped = function (dragElID, dropElID, dragElName, dropElName) {
            var droppedGroup = $filter('filter')($scope.groups, { groupid: dropElID })[0];
            var droppedUser;
                var isCheck = true;
                var mainOrNot=false;
                angular.forEach(droppedGroup.lists, function (key, value) {
                   
                   
                    if(isCheck==true){
                    if (key.main == false) {
                        droppedUser = $filter('filter')(key.people, { userid: dragElID })[0];
                        mainOrNot = true;
                    }
                    else {
                        droppedUser = $filter('filter')(key.people, { name: dragElName })[0];

                    }
                    if (droppedUser != undefined || droppedUser != null)
                        isCheck = false;
                    }

               
                   
                        
                })
               
                if (droppedUser != undefined || droppedUser != null)
                    alert("This user is already present in this group.")
                else
                {
                    var userDropped = $filter('filter')($rootScope.AllUsers, { userid: dragElID })[0];
                    var user;
                    var list;
                    if(mainOrNot==true)
                    {
                        list = $filter('filter')(droppedGroup.lists, { main: true })[0];
                        user = { "name": userDropped.username, "type": "user", "avatar": "assets/images/avatars/andrew.jpg" }
                       
                        
                    }
                    else
                    {
                        list = $filter('filter')(droppedGroup.lists, { main: false })[0];
                        user = { "name": userDropped, "userid": userDropped.userid, "type": "user", "avatar": "assets/images/avatars/andrew.jpg" }
                       
                    }
                    list.people.push(user);
                    zmProfileGroupsService.addUserToGroup($scope.groups).then(function (response) {
                        
                    },
                    function (a, b, c) {
                        
                    })
                }
             

               
            };
        })
}])
    //group user Controller
    .controller('GroupUserCrtl', ["zmProfileGroupsService", "$scope", "$rootScope", function CountCtrl(zmProfileGroupsService, $scope, $rootScope) {
        
        zmProfileGroupsService.readGroupUsers().then(function (response) {
            
            $scope.groupsUsers = response.data;
            $rootScope.AllUsers = response.data;
        })
    }])
   //component for groups
	.component('zmProfileGroups', {
	    bindings: {
	        userid: '='
	    },
	   controller:'GroupCrtl',
	   templateUrl: 'profile/tabs/group/GroupComponent.html'
	})
    //component for  users
.component('zmProfileGroupsDetail', {
    bindings: {
        users: '='
    },
    controller:'GroupUserCrtl',
    templateUrl: 'profile/tabs/group/GroupUsersComponent.html'
})
    //component for groups details
.component('zmProfileGroupsGroupListItem', {
    bindings: {
        group: '='
    },
    controller: 'GroupCrtl',
    templateUrl: 'profile/tabs/group/GroupDetailComponent.html'
})
;

