﻿
//rest service to get the data from json files
app.factory('zmProfileGroupsRestService', ['$http', '$q', function ($http, $q) {
    var authServiceFactory = {};
    var serviceBase = window.location.protocol + '//' + window.location.host + '/';
    //read list of groups
     var _readListOfGroups = function (userId) {
     var deferred = $q.defer();
     $http({

          method: 'GET',
          url: serviceBase +"groups.json",
          
         
      }).success(function (data) {
          deferred.resolve(data);
      }).error(function (error) {
          deferred.reject(error);
      });
      return deferred.promise;
    }

   
    //read list of group users
    var _readGroupUsers = function () {
       var deferred = $q.defer();
       $http({

            method: 'GET',
            url: serviceBase +"contacts.json",

           
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

   
    authServiceFactory.readListOfGroups = _readListOfGroups;
 
    authServiceFactory.readGroupUsers = _readGroupUsers;
  
    return authServiceFactory;

}]);