﻿app.factory('zmProfileGroupsService', ["zmProfileGroupsRestService",  '$q', function (zmProfileGroupsRestService,$q) {
    var authServiceFactory = {};
    //read the list of groups 
    var _readListOfGroups = function (userId) {
        var deferred = $q.defer();

        zmProfileGroupsRestService.readListOfGroups().then(function (response) {
            deferred.resolve(response);
        },
        function (error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }
    //read the list of group users
 var _readGroupUsers = function () {

        var deferred = $q.defer();
        zmProfileGroupsRestService.readGroupUsers().then(function (data) {
            deferred.resolve(data);
        }, function (error) {
            deferred.reject(error);
        })
      
        return deferred.promise;
    }
    authServiceFactory.readListOfGroups = _readListOfGroups;
    authServiceFactory.readGroupUsers = _readGroupUsers;
    return authServiceFactory;

}]);