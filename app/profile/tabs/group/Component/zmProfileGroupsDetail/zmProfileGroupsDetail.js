﻿
//component for  users
app.component('zmProfileGroupsDetail', {
    bindings: {
        users: '='
    },
    controller:'GroupUserCrtl',
    templateUrl: 'profile/tabs/group/Component/zmProfileGroupsDetail/GroupUsersComponent.html'
})

 //group user Controller
    .controller('GroupUserCrtl', ["zmProfileGroupsService", "$scope", "$rootScope", function CountCtrl(zmProfileGroupsService, $scope, $rootScope) {

        zmProfileGroupsService.readGroupUsers().then(function (response) {

            $scope.groupsUsers = response.data;
            $rootScope.AllUsers = response.data;
        })
    }])