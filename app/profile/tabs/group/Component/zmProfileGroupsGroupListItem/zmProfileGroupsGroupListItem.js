﻿//component for groups details
app.component('zmProfileGroupsGroupListItem', {
    bindings: {
        group: '='
    },
    templateUrl: 'profile/tabs/group/Component/zmProfileGroupsGroupListItem/GroupDetailComponent.html'
})

