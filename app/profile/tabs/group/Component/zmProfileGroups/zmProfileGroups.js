﻿//Group controller
app.controller('GroupCrtl', ["zmProfileGroupsService", "$scope", "$rootScope", "$filter", "$mdDialog", function CountCtrl(zmProfileGroupsService, $scope, $rootScope, $filter, $mdDialog) {
    zmProfileGroupsService.readListOfGroups().then(function (response) {
        $scope.groups = response.data;
        $rootScope.AllGroups = $scope.groups;
    })
    //function for drop the user in group
    $rootScope.dropped = function (dragElID, dropElID, dragElName, dropElName) {
        debugger;
        var droppedGroup = $filter('filter')($scope.groups, { groupname: dropElName })[0];
        var droppedUser;
        var isCheck = true;
        var mainOrNot = false;
        angular.forEach(droppedGroup.lists, function (key, value) {
           

            if (isCheck == true) {
                if (key.main == false) {
                    droppedUser = $filter('filter')(key.people, { userid: dragElID })[0];
                    if (droppedUser != undefined) {
                        mainOrNot = true;
                    }
                   
                }
                else {
                    droppedUser = $filter('filter')(key.people, { name: dragElName })[0];

                }
                if (droppedUser != undefined || droppedUser != null)
                    isCheck = false;
            }




        })
        debugger;
        if (droppedUser != undefined || droppedUser != null)
            alert("This user is already present in this group.")
        else {
            var userDropped = $filter('filter')($rootScope.AllUsers, { userid: dragElID })[0];
            var user;
            var list;
            if (mainOrNot == true) {
                list = $filter('filter')(droppedGroup.lists, { main: true })[0];
                user = { "name": userDropped.username, "type": "user", "avatar": "assets/images/avatars/andrew.jpg" }


            }
            else {
                list = $filter('filter')(droppedGroup.lists, { main: false })[0];
                user = { "name": userDropped.username, "userid": userDropped.userid, "type": "user", "avatar": "assets/images/avatars/andrew.jpg" }

            }
            list.people.push(user);


        }



    };


    //function for remove the user from group
    $scope.deleteUser = function (name, groupid) {
        debugger;
        
        //$mdDialog.show({
        //    controller: 'DeleteUserDialogController',
        //    controllerAs: 'vm',
        //    templateUrl: 'profile/dialogs/delete-user/delete-user.html',
        //    //targetEvent: ev,
        //    clickOutsideToClose: true
        //}).then(function (response) {
        //    debugger;
        //    if (response == true) {
                var mainOrNot = false;
                var droppedGroup = $filter('filter')($scope.groups, { groupid: groupid })[0];
                var isCheck = true;

                angular.forEach(droppedGroup.lists, function (key, value) {

                    debugger;
                    if (isCheck == true) {
                        if (key.main == false) {
                            droppedUser = $filter('filter')(key.people, { name: name })[0];
                            if (droppedUser != undefined || droppedUser != null)
                                mainOrNot = true;
                        }
                        else {
                            droppedUser = $filter('filter')(key.people, { name: name })[0];

                        }
                        if (droppedUser != undefined || droppedUser != null)
                            isCheck = false;
                    }




                })
                var index;
                var list;

                if (mainOrNot == true) {
                    list = $filter('filter')(droppedGroup.lists, { main: false })[0];
                    index = list.people.indexOf(droppedUser);

                }
                else {
                    list = $filter('filter')(droppedGroup.lists, { main: true })[0];
                    index = list.people.indexOf(droppedUser);

                }

                list.people.splice(index, 1);
                var aa = $scope.groups;
           // }
            //}, function () {
            //});
        
      
    }
    $scope.DeleteConfirm = function () {

    }

    //add new group
    $scope.addGroup=function() {
        debugger;
        $mdDialog.show({
            controller: 'NewGroupDialogController',
            controllerAs: 'vm',
            templateUrl: 'profile/dialogs/new-group/new-group.html',
            //targetEvent: ev,
            clickOutsideToClose: true
        }).then(function () {
            debugger;
           // $scope.groups = $rootScope.AllG;
        }, function () {
        });
    }

    //function for delete group
    $scope.deleteGroup = function (groupid) {
       
        $rootScope.grouptodelete = groupid;
        $mdDialog.show({
            controller: 'DeleteGroupDialogController',
            controllerAs: 'vm',
            templateUrl: 'profile/dialogs/delete-group/delete-group.html',
          
            clickOutsideToClose: true
        }).then(function () {
           
           
        }, function () {
        });
    }

    $scope.updateGroup = function (group) {
        debugger;
        $rootScope.groupDetails = group;
        $mdDialog.show({
            controller: 'EditGroupDialogController',
            controllerAs: 'vm',
            templateUrl: 'profile/dialogs/edit-group/edit-group.html',
            //targetEvent: ev,
            clickOutsideToClose: true
        }).then(function () {
            debugger;
            // $scope.groups = $rootScope.AllG;
        }, function () {
        });
    }
}])

 //component for groups
	.component('zmProfileGroups', {
	    bindings: {
	        userid: '='
	    },
	    controller: 'GroupCrtl',
	    templateUrl: 'profile/tabs/group/Component/zmProfileGroups/GroupComponent.html'
	})