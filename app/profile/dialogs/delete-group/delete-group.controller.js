(function ()
{
  'use strict';

 
   app.controller('DeleteGroupDialogController', DeleteGroupDialogController);

  /** @ngInject */
   function DeleteGroupDialogController($mdDialog, $scope, $rootScope, $filter)
  {
    var vm = this;

    // Data

    //////////

    // Methods
    vm.closeDialog = closeDialog;

    //////////

    function closeDialog()
    {
      $mdDialog.hide();
    }
    $scope.deleteGroup = function () {
       
        var groupid = $rootScope.grouptodelete;
       
        var index = $rootScope.AllGroups.indexOf(groupid);
        $rootScope.AllGroups.splice(index, 1);
        $mdDialog.hide();
    }
  }
})();
