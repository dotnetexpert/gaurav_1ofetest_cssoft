(function ()
{
  'use strict';

 
    app.controller('DeleteUserDialogController', DeleteUserDialogController);

  /** @ngInject */
    function DeleteUserDialogController($mdDialog, $scope)
    {

    var vm = this;

    // Data

    //////////

    // Methods
    vm.closeDialog = closeDialog;

    //////////

    function closeDialog()
    {

        $mdDialog.hide();
        return false;
    }
    $scope.deleteUser = function () {

        $mdDialog.hide();
        return true;
    }
  }
})();
